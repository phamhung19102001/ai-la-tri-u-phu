package com.hung.ailatrieuphu.view.act;


import com.hung.ailatrieuphu.databinding.ActivityHomeBinding;
import com.hung.ailatrieuphu.view.fragment.M000MenuFrg;
import com.hung.ailatrieuphu.viewmodel.CommonVM;


public class HomeActivity extends BaseAct<ActivityHomeBinding, CommonVM> {

    @Override
    protected void initViews() {
        showFragment(M000MenuFrg.TAG, null, false);
    }

    @Override
    protected ActivityHomeBinding initViewBinding() {
        return ActivityHomeBinding.inflate(getLayoutInflater());
    }

    @Override
    protected Class<CommonVM> initViewModel() {
        return CommonVM.class;
    }
}