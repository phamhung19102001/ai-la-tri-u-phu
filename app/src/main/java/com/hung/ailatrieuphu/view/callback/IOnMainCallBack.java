package com.hung.ailatrieuphu.view.callback;

public interface IOnMainCallBack {
    void showFragment(String tag, Object data, boolean isBack);

    void backToPrevious();
}
