package com.hung.ailatrieuphu.view.dialog;

import android.content.Context;

import androidx.annotation.NonNull;

import com.hung.ailatrieuphu.databinding.ViewInfoAppDialogBinding;

public class InfoAppDialog extends BaseDialog<ViewInfoAppDialogBinding> {
    public InfoAppDialog(@NonNull Context context) {
        super(context, null);
    }

    @Override
    protected void initData(Object[] data) {

    }

    @Override
    protected void initView() {
        setCancelable(true);
        setCanceledOnTouchOutside(true);
    }

    @Override
    protected ViewInfoAppDialogBinding initViewBinding() {
        return ViewInfoAppDialogBinding.inflate(getLayoutInflater());
    }
}
