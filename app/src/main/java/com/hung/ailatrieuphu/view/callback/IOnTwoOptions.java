package com.hung.ailatrieuphu.view.callback;

public interface IOnTwoOptions {
    void doPositiveAction();
    void doNegativeAction();
}
