package com.hung.ailatrieuphu.view.callback;

public interface IOnBackPressed {
    boolean onBackPressed();
}
